package main

import (
	"flag"
	"os"

	"bitbucket.org/coh/dpmastergo/dpmastergo"
)

type Options struct {
	list   bool   //list bans
	add    string //add ban
	del    string //remove ban
	dbUrl  string //backend url
	dbAuth string //backend auth if not included in dbUrl
}

type Ban struct {
	cidr   string
	reason string
}

func ParseOptions() *Options {
    usage := `dpmg-bans.

Usage:
  dpmg-bans
`
}

func main() {
	opts := ParseOptions()

	if opts.list {
		ListBans(opts)
	} else if opts.add != "" {
		AddBan(opts.add)
	} else {
		RemoveBan(opts.del)
	}
}

func ListBans(opts *Options) {
	dpmastergo.NewBackend(*opts)
}

func SaveBans(opts *Options) {
	db, err := dpmastergo.NewRedisBackend(opts.dbUrl, opts.dbAuth)
	dpmastergo.OkOrBail(err)
}
