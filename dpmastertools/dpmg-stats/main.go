package main

import (
	"fmt"
	"math"
	"net"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/docopt/docopt-go"
	"github.com/kdar/factorlog"
	"github.com/ziutek/rrd"

	"bitbucket.org/coh/dpmastergo/dpmastergo"
)

type Options struct {
	dbUrl      string
	dbAuth     string
	duration   time.Duration
	samples    int
	outputPath string
}

var log *factorlog.FactorLog = nil

func commonInit() {
	formatstring := `%{Color "magenta" "PANIC"}%{Color "magenta" "FATAL"}%{Color "red" "ERROR"}%{Color "yellow" "WARN"}%{Color "green" "INFO"}%{Color "cyan" "DEBUG"}%{Color "blue" "TRACE"}[%{Time}] [%{Severity}:%{ShortFile}:%{Line}] %{SafeMessage}%{Color "reset"}`
	log = factorlog.New(os.Stderr, factorlog.NewStdFormatter(formatstring))
}

func NewLog(o *Options) {
	if log == nil {
		commonInit()
		log.SetMinMaxSeverity(factorlog.Severity(factorlog.DEBUG), factorlog.PANIC)
	}
}

func parseOptions() (*Options, error) {
	usage := `dpmg-stats.

Usage:
  dpmg-stats [options]

Options:
  --db-auth=DBAUTH            specify authentication for connecting to the backend [default: ]
  --db-url=DBURL              specify url for connecting to the backend. [default: localhost:6379]
  --stat-interval=T           how long to wait to sample again (should be <= .5 heartbeat rate of the game servers) [default: 60s]
  -o PATH --output-path=PATH  output path for the generated graphs [default: ./graphs/]
  --samples=N                 samples to accumulate over [default: 15]
  --version                   display version and exit
  --help -h                   show this help and exit
`
	arguments, err := docopt.Parse(usage, nil, true, "dpmg-stats "+dpmastergo.FullVersion, false)
	dpmastergo.OkOrBail(err)

	duration, err := time.ParseDuration(arguments["--stat-interval"].(string))
	dpmastergo.OkOrBail(err)

	samples, err := strconv.Atoi(arguments["--samples"].(string))
	dpmastergo.OkOrBail(err)

	path, err := filepath.Abs(arguments["--output-path"].(string))
	dpmastergo.OkOrBail(err)

	return &Options{
		dbAuth:     arguments["--db-auth"].(string),
		dbUrl:      arguments["--db-url"].(string),
		duration:   duration,
		samples:    samples,
		outputPath: path,
	}, nil
}

type StatSink interface {
	Write(timestamp time.Time, key, value []byte)
	Close()
}

const gamename = "Vecxis"

var keys []string = []string{"botCount", "playerCount", "serversTotal"}

type StatCollector func(timestamp time.Time, backend dpmastergo.Backend, sink StatSink)

func PlayerAndBotCount(backend dpmastergo.Backend, serverPool []net.UDPAddr) (playerCount, botCount uint64) {
	playerCount = 0
	botCount = 0

	for _, server := range serverPool {
		status, _ := dpmastergo.ParseInfoResponseString(
			backend.GetServerStatus(server.String(), gamename))
		bots, _ := strconv.ParseUint(status["bots"], 10, 64)
		players, _ := strconv.ParseUint(status["clients"], 10, 64)
		players -= bots

		playerCount += players
		botCount += bots
	}

	return
}

func ServerCount(_ dpmastergo.Backend, serverPool []net.UDPAddr) uint64 {
	return uint64(len(serverPool))
}

func collectStats(timestamp time.Time, backend dpmastergo.Backend, sink *rrd.Updater) {
	results := make(map[string]string)

	serverPool := backend.GetServerPool(true, true, gamename)

	results["serversTotal"] = strconv.FormatUint(ServerCount(backend, serverPool), 10)

	p, b := PlayerAndBotCount(backend, serverPool)
	results["playerCount"], results["botCount"] = strconv.FormatUint(p, 10), strconv.FormatUint(b, 10)

	sink.SetTemplate(keys...)
	err := sink.Update(timestamp, results["botCount"], results["playerCount"], results["serversTotal"])
	dpmastergo.OkOrBail(err)
}

const (
	statsFilename = "dpmaster.rrd"
)

func generateGraphs(now time.Time, options *Options) {
	intervals := make(map[string]time.Duration)

	intervals["hourly"] = -1 * time.Hour
	intervals["daily"] = -24 * time.Hour
	intervals["weekly"] = 7 * -24 * time.Hour

	for _, key := range keys {
		for dname, duration := range intervals {
			g := rrd.NewGrapher()
			g.SetSize(512, 384)
			g.Def("avg", statsFilename, key, "AVERAGE")
			g.Def("max", statsFilename, key, "MAX")
			g.Line(2, "avg", "00ff00", "average")
			g.Line(1, "max", "0000ff", "max")
			g.SetTitle(dname + " " + key)
			_, err := g.SaveGraph(filepath.Join(options.outputPath, key+"-"+dname+".png"), time.Now().Add(duration), now)
			dpmastergo.OkOrLog(err)
		}
	}

	fmt.Printf("finished rendering @ %s, took %f\n", now,
		time.Now().Sub(now).Seconds())
	os.Stdout.Sync()

}

func main() {
	options, err := parseOptions()
	dpmastergo.OkOrBail(err)

	log := dpmastergo.NewLog(os.Stderr, factorlog.INFO)
	dpmastergo.InitLogging(log)

	db, err := dpmastergo.NewRedisBackend(options.dbUrl, options.dbAuth)
	dpmastergo.OkOrBail(err)

	create := rrd.NewCreator(statsFilename, time.Now(), 2+uint(options.duration.Seconds()))

	week := 7 * 24 * 1 * time.Hour
	samplesToKeep := int(math.Ceil(week.Seconds() / options.duration.Seconds()))

	//new data series
	create.DS("serversTotal", "GAUGE", 4+2*uint(options.duration.Seconds()), 0, "U")
	create.DS("playerCount", "GAUGE", 4+2*uint(options.duration.Seconds()), 0, "U")
	create.DS("botCount", "GAUGE", 4+2*uint(options.duration.Seconds()), 0, "U")
	//at most .1 = 10% unknown values are permitted for a good result
	//by default, average over the past 30 samples = last hour (30 * 120 = 3600)
	create.RRA("AVERAGE", 0.1, options.samples, samplesToKeep)
	create.RRA("MAX", 0.9, 1, samplesToKeep)

	create.Create(false)

	receiver := rrd.NewUpdater(statsFilename)

	collect := time.Tick(options.duration)
	count := 0
	for now := range collect {
		collectStats(now, db, receiver)
		count += 1

		if count == 5 {
			generateGraphs(now, options)
			count = 0
		}
	}

}
