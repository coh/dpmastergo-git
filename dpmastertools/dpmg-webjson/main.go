package main

import (
	"encoding/binary"
	"encoding/json"
	"net"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/docopt/docopt-go"
	"github.com/kdar/factorlog"

	"bitbucket.org/coh/dpmastergo/dpmastergo"
)

type Options struct {
	dbUrl           string             // the url for the storage backend
	dbAuth          string             // the auth for the storage backend, if not included in backendUrl
	listenUrl       string             // the listen url for the web frontend
	minimumLoglevel factorlog.Severity // the minimum log level
}

type State struct {
	cache     map[string][]byte
	cacheAges map[string]time.Time
	mutex     sync.RWMutex
	db        dpmastergo.Backend
}

const (
	maxCacheAge = 1 * time.Minute
)

func parse_port(port []byte) string {
	return strconv.FormatUint(uint64(binary.BigEndian.Uint16(port)), 10)
}

func parse_ip(address []byte) string {
	if len(address) == 4+2 {
		return net.IPv4(address[0], address[1], address[2], address[3]).String() + ":" + parse_port(address[4:])
	} else {
		return "[" + net.IP(address[:len(address)-2]).String() + "]:" + parse_port(address[len(address)-2:])
	}
}

type handlerFunc func(w http.ResponseWriter, req *http.Request)

func createHandleServerMeta(s *State) handlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		//TODO
	}
}

func getServerInfoList(s *State, includeEmpty, includeFull bool, gamename string) []map[string]string {
	ips := s.db.GetServerPool(includeEmpty, includeFull, gamename)

	serverlist := make([]map[string]string, len(ips))

	for i, ip := range ips {
		serverlist[i], _ = dpmastergo.ParseInfoResponseString(s.db.GetServerStatus(ip.String(), gamename))
		serverlist[i]["ip"] = ip.String()

		delete(serverlist[i], "challenge")
	}

	return serverlist
}

func isValidGame(gamename string) bool {
	validGames := []string{"Vecxis", "Warsow"}

	//manual search as the sort.SearchStrings is not the functionality
	//we want
	for _, game := range validGames {
		if gamename == game {
			return true
		}
	}

	return false
}

func sendCannedResponseIfBestBefore(s *State, w http.ResponseWriter, req *http.Request) bool {
	s.mutex.RLock()
	timestamp := s.cacheAges[req.Form.Encode()]
	cachedReply, cacheExists := s.cache[req.Form.Encode()]
	s.mutex.RUnlock()

	if time.Now().Sub(timestamp) <= maxCacheAge && cacheExists {
		w.Write(cachedReply)
		return true
	} else {
		s.mutex.Lock()
		return false
	}
}

func canNewResponse(s *State, w http.ResponseWriter, req *http.Request, response []byte) {
	s.cacheAges[req.Form.Encode()] = time.Now()
	s.cache[req.Form.Encode()] = response
	s.mutex.Unlock()
	w.Write(response)
}

func enforceWhitelistedFormValues(req *http.Request) {
	goodKeys := map[string]bool{"game": true, "empty": true, "full": true}

	for key := range req.Form {
		if !goodKeys[key] {
			delete(req.Form, key)
		}
	}
}

func createHandleServerlist(s *State) handlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		if req.Method != "GET" || !isValidGame(req.FormValue("game")) {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		enforceWhitelistedFormValues(req)

		if !sendCannedResponseIfBestBefore(s, w, req) {
			_, includeEmpty := req.Form["empty"]
			_, includeFull := req.Form["full"]

			jsondata, _ := json.Marshal(getServerInfoList(s, includeEmpty, includeFull, req.FormValue("game")))

			canNewResponse(s, w, req, jsondata)
		}
	}
}

var log *factorlog.FactorLog = nil

func RunWebFrontend(options *Options) {
	s := new(State)
	s.cache = make(map[string][]byte)
	s.cacheAges = make(map[string]time.Time)
	var err error
	s.db, err = dpmastergo.NewRedisBackend(options.dbUrl, options.dbAuth)
	dpmastergo.OkOrBail(err)
	http.HandleFunc("/stats", createHandleServerMeta(s))
	http.HandleFunc("/", createHandleServerlist(s))

	http.ListenAndServe(options.listenUrl, nil)
}

func printSelfAlloc() {
	var mstat runtime.MemStats
	tock := time.Tick(1 * time.Minute)
	for _ = range tock {
		runtime.ReadMemStats(&mstat)
		log.Debugln("self stats: Alloc ", mstat.Sys)
	}
}

func ParseOptions() (*Options, error) {
	usage := `dpmg-webjson.

Usage:
  dpmg-webjson [options]

Options:
  --db-auth=DBAUTH            specify authentication for connecting to the backend [default: ]
  --db-url=DBURL              specify url for connecting to the backend. [default: localhost:6379]
  --listen=INTERFACE_OR_URL   listen on the given interface [default: :8080]
  --loglevel=LOGLEVEL         minimum log level [default: INFO]
`

	arguments, err := docopt.Parse(usage, nil, true, "dpmg-webjson "+dpmastergo.FullVersion, false)
	dpmastergo.OkOrBail(err)

	return &Options{
		dbAuth:          arguments["--db-auth"].(string),
		dbUrl:           arguments["--db-url"].(string),
		listenUrl:       arguments["--listen"].(string),
		minimumLoglevel: factorlog.StringToSeverity(strings.ToUpper(arguments["--loglevel"].(string))),
	}, nil
}

func main() {
	options, _ := ParseOptions()
	log = dpmastergo.NewLog(os.Stderr, options.minimumLoglevel)
	dpmastergo.InitLogging(log)
	//	go printSelfAlloc()
	RunWebFrontend(options)
}
