package chacha_prng

import (
	"crypto/sha512"
	"encoding/binary"
	"hash"
	"math"
	"sync"
)

type ChaChaPrng struct {
	core                    chaChaCore
	used_bytes              uint64
	requests_without_reseed uint64
	reseed_pool             hash.Hash
	result_buffer           []byte
	lock                    sync.Mutex
}

func NewPrng(key []byte, nonce []byte) *ChaChaPrng {
	result := new(ChaChaPrng)
	result.Reseed(key, nonce)
	return result
}

func (p *ChaChaPrng) Reseed(key []byte, nonce []byte) {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.core.reseed(key, nonce)
	p.reseed_pool = sha512.New()
	p.requests_without_reseed = 0
	p.used_bytes = 0
	p.result_buffer = make([]byte, 0, 1<<16)
}

func (p *ChaChaPrng) generate_bytes() {
	if len(p.result_buffer) >= 16 {
		return
	}
	result := p.core.generate_bytes()
	if p.core.state[12] == 0 && p.core.state[13] == 0 {
		p.reseedFromPool()
	}

	p.result_buffer = append(p.result_buffer, result[0:]...)
	p.used_bytes += 64
}

func (p *ChaChaPrng) reseedFromPool() {
	tmp := p.reseed_pool.Sum(nil)
	p.Reseed(tmp[:32], tmp[32:])
}

func (p *ChaChaPrng) generate_uint64() uint64 {
	if len(p.result_buffer) < 8 {
		p.generate_bytes()
	}

	result := binary.LittleEndian.Uint64(p.result_buffer[0:])
	p.result_buffer = p.result_buffer[8:]

	return result
}

func (p *ChaChaPrng) step() uint64 {
	result := p.generate_uint64()

	p.requests_without_reseed += 1
	if p.requests_without_reseed == 1<<48 {
		p.reseedFromPool()
	} else if p.requests_without_reseed%(1<<16) == 0 {
		p.generate_bytes()
		p.reseed_pool.Write(p.result_buffer[p.requests_without_reseed/(1<<16)%64:])
		p.result_buffer = make([]byte, 0, 1<<16)
	}

	return result
}

func (p *ChaChaPrng) Read(b []byte) (n int, err error) {
	p.lock.Lock()
	defer p.lock.Unlock()
	blen := len(b)
	len_ := 0

	for len_ < blen {
		copy(b[len_:], p.result_buffer)
		len_ += len(p.result_buffer)
		p.result_buffer = make([]byte, 0, 1<<16)
		p.generate_bytes()
	}

	return len(b), nil
}

func (p *ChaChaPrng) Seed(seed int64) {
	p.lock.Lock()
	defer p.lock.Unlock()
	iv := make([]byte, 8)
	binary.LittleEndian.PutUint64(iv[0:], uint64(seed))
	key := make([]byte, 16)
	binary.LittleEndian.PutUint64(key[0:], uint64(seed))
	binary.LittleEndian.PutUint64(key[8:], uint64(seed))

	p.Reseed(key, iv)
}

func (p *ChaChaPrng) Int63() int64 {
	p.lock.Lock()
	defer p.lock.Unlock()
	var tmp int64 = -1
	for tmp < 0 || tmp >= math.MaxInt64 {
		tmp = int64(p.step() % (1 << 63))
	}

	return tmp
}

func (p *ChaChaPrng) IntRange(min, max uint64) uint64 {
	p.lock.Lock()
	defer p.lock.Unlock()
	range_bits := uint64(math.Ceil(math.Log2(float64(max-min) + 1.0)))
	range_ := max - min + 1

	var mask uint64

	if range_bits == 64 {
		mask = 0xffffffffffffffff
	} else {
		mask = (1 << range_bits) - 1
	}

	for {
		res := p.generate_uint64() & mask

		if res <= range_ {
			return res + min
		}
	}
}
