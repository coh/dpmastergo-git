package chacha_prng

import "testing"

func TestCoreReseed(t *testing.T) {
	var c chaChaCore

	for i := 0; i < 64; i++ {
		if i == 16 || i == 32 {
			continue
		}

		err := c.reseed(make([]byte, i), make([]byte, 8))
		if err != ErrInvalidKey {
			t.Error("expected ErrInvalidKey, got ", err)
		}
	}

	for i := 0; i < 16; i++ {
		if i == 8 {
			continue
		}

		err := c.reseed(make([]byte, 16), make([]byte, i))
		if err != ErrInvalidNonce {
			t.Error("expected ErrInvalidNonce, got ", err)
		}
	}

	for i := 1; i <= 2; i++ {
		err := c.reseed(make([]byte, 16*i), make([]byte, 8))

		if err != nil {
			t.Error("expected nil, got ", err)
		}
	}

}

func TestCoreConstants(t *testing.T) {
	var c, d chaChaCore

	err := c.reseed(make([]byte, 16), make([]byte, 8))
	if err != nil {
		t.Error("expected nil, got ", err)
	}

	err = d.reseed(make([]byte, 32), make([]byte, 8))
	if err != nil {
		t.Error("expected nil, got ", err)
	}

	same := true

	for i := 0; i < chaChaStateSize; i++ {
		same = same && (c.state[i] == d.state[i])
	}

	if same {
		t.Error("expected different constants, got the same one")
	}
}

func TestCoreReseedValues(t *testing.T) {
	var c, d chaChaCore
	c.reseed(make([]byte, 32), make([]byte, 8))

	if c.state[0] != 0x61707865 {
		t.Errorf("state[0] != 0x61707865: is %x", c.state[0])
	}

	if c.state[1] != 0x3320646e {
		t.Errorf("state[1] != 0x3120646e: is %x", c.state[1])
	}

	if c.state[2] != 0x79622d32 {
		t.Errorf("state[2] != 0x79622d36: is %x", c.state[2])
	}

	if c.state[3] != 0x6b206574 {
		t.Errorf("state[3] != 0x6b206574: is %x", c.state[3])
	}

	d.reseed(make([]byte, 16), make([]byte, 8))
	if d.state[0] != 0x61707865 {
		t.Errorf("state[0] != 0x61707865: is %x", d.state[0])
	}

	if d.state[1] != 0x3120646e {
		t.Errorf("state[1] != 0x3120646e: is %x", d.state[1])
	}

	if d.state[2] != 0x79622d36 {
		t.Errorf("state[2] != 0x79622d36: is %x", d.state[2])
	}

	if d.state[3] != 0x6b206574 {
		t.Errorf("state[3] != 0x6b206574: is %x", d.state[3])
	}
}
