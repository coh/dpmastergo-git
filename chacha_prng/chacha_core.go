package chacha_prng

/* implements the core ChaCha-20 algorithm */

import (
	"encoding/binary"
	"errors"
	"fmt"
)

const chaChaStateSize = 16

var (
	ErrInvalidKey   = errors.New("Invalid key length: must be either 128 or 256 bits")
	ErrInvalidNonce = errors.New("Invalid nonce length: must be 64 bits")
)

type chaChaCore struct {
	state [chaChaStateSize]uint32
}

func (c *chaChaCore) reseed(key []byte, nonce []byte) (err error) {
	if len(key) != 16 && len(key) != 32 {
		return ErrInvalidKey
	} else if len(nonce) != 8 {
		return ErrInvalidNonce
	}

	for i := 0; i < len(c.state); i += 1 {
		c.state[i] = 0
	}

	constant := []byte(fmt.Sprintf("expand %02d-byte k", len(key)))

	for i := 0; i < 4; i += 1 {
		c.state[0+i] = binary.LittleEndian.Uint32(constant[4*i:])
		c.state[4+i] = binary.LittleEndian.Uint32(key[4*i:])
		c.state[8+i] = binary.LittleEndian.Uint32(key[16%len(key)+4*i:])
	}
	c.state[12] = 0
	c.state[13] = 0
	c.state[14] = binary.LittleEndian.Uint32(nonce)
	c.state[15] = binary.LittleEndian.Uint32(nonce[4:])

	return nil
}

func rotl(x uint32, shift uint) uint32 {
	return (x << shift) | (x >> (32 - shift))
}

func quarter_round(x []uint32, a int, b int, c int, d int) {
	x[a] += x[b]
	x[d] ^= x[a]
	x[d] = rotl(x[d], 16)
	x[c] += x[d]
	x[b] ^= x[c]
	x[b] = rotl(x[b], 12)
	x[a] += x[b]
	x[d] ^= x[a]
	x[d] = rotl(x[d], 8)
	x[c] += x[d]
	x[b] ^= x[c]
	x[b] = rotl(x[b], 7)
}

//the equivalent to salsa20_wordtobyte
func (c *chaChaCore) generate_bytes() [64]byte {
	var result [64]byte
	x := make([]uint32, 16)
	copy(x, c.state[0:])

	for i := 0; i < 8; i += 2 {
		quarter_round(x, 0, 4, 8, 12)
		quarter_round(x, 1, 5, 9, 13)
		quarter_round(x, 2, 6, 10, 14)
		quarter_round(x, 3, 7, 11, 15)
		quarter_round(x, 0, 5, 10, 15)
		quarter_round(x, 1, 6, 11, 12)
		quarter_round(x, 2, 7, 8, 13)
		quarter_round(x, 3, 4, 9, 14)
	}

	for i := 0; i < 16; i += 1 {
		x[i] += c.state[i]
	}

	c.state[12] += 1
	if c.state[12] == 0 {
		c.state[13] += 1
	}

	for i := 0; i < 16; i += 1 {
		binary.LittleEndian.PutUint32(result[4*i:], x[i])
	}

	return result
}
