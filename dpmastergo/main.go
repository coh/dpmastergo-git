package main

import dp "bitbucket.org/coh/dpmastergo/dpmastergo"

func main() {
	options := dp.ParseOpts()
	state, err := dp.NewState(options)

	dp.OkOrBail(err)

	dp.InitLogging(state.Logger)

	dp.InitNetwork(state, options)

	//	UnsecureInit(options)
	//	SecureInit(options)

	//	fmt.Println(options)

	//don't for {} this unless you enjoy a fork bomb
	dp.Run(state, options)
	//	dp.ListenOnState(state)
}
