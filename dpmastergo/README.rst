Architecture
============

There's one coroutine per listen interface. Each incoming packet is handled in
its own coroutine.

The backend is (currently) using Redis_.

.. _Redis: http://redis.io

Redis supports a configurable amount of (table-less) databases, of which we'll
make use; namely:

   1. .. _1:

      Database 0 is used for Challenges as part of the Heartbeat system.
   2. .. _2:
         
      Database 1 is used to store the Server information as received through a
      Heartbeat challenge response.
   3. .. _3:
         
      Database 2 is used to store the query origins that are misbehaving
   4. .. _4:
        
      Database 3 is used to store the origins that are to be denied any
      sorts of interaction with the master server.

In case of 1_., the challenge (byte-)string is the key with the Address being
the value -- that way, it is easy to verify a challenge response.

In case of 3_., the key is the origin address and the value's an integer. On
each request, the integer is increased by one. If the value's above a certain
threshold, then any additional packet will be dropped. Should there be no
request within a certain amount of time, the key will be auto-removed, and
packets from that origin will be processed again.

In case of 4_., all the ban data are stored in a set as CIDR strings.
