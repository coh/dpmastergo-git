package dpmastergo

import "time"

const (
	//min/max challenge lengths, excluding any trailing '\0'
	MinChallengeLength = 8
	MaxChallengeLength = 11
)

const (
	//max length for a game name in a request
	MaxGameNameLength = 64
	//max length for a game type/mode name in a request
	MaxGameTypeLength = 32
)

const (
	//don't include empty nor full servers in the response
	GameOptionNone = 0
	//include empty servers in the response
	GameOptionSendEmptyServers = (1 << iota)
	//include full servers in the response
	GameOptionSendFullServers = (1 << iota)
)

const (
	ChallengeTimeout = 2 * time.Second
	StatusTimeout    = 20 * time.Minute
)

const (
	FullVersion = Version + " (hg rev. " + Revision + ") " + BuildTime
	//Version, Revision, and BuildTime are generated during the build
)
