package dpmastergo

import "net"

type CsPrng interface {
	Reseed(key, nonce []byte)
	Read(b []byte) (int, error)
	Seed(seed int64)
	Int63() int64
	IntRange(min, max uint64) uint64
}

type Backend interface {
	StoreHeartbeatChallenge(rng CsPrng, payload string) (challenge []byte)
	PayloadForHeartbeatChallenge(_ CsPrng, challenge []byte) (payload string)
	AddServerToPool(gamename string, inforesponse []byte, remoteAddress string)
	GetServerPool(includeEmpty, includeFull bool, gamename string) (serverpool []net.UDPAddr)
	GetServerStatus(address, gamename string) []byte
	IsIgnored(ip net.UDPAddr) bool
	IsBanned(ip net.UDPAddr) bool
}
