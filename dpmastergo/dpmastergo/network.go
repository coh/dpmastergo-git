package dpmastergo

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"net"
	"os"
	"strconv"
	"text/tabwriter"
	"time"
)

type messageHandler func(local *net.UDPConn, s *State, remote *net.UDPAddr, msg []byte)

const (
	separator                                = "\\"
	packetPrefix                             = "\xff\xff\xff\xff"
	darkplacesString                         = "DarkPlaces\n"
	serverToMasterHeartbeat                  = "heartbeat"
	masterToServerGetInfo                    = "getinfo "
	serverToMasterInfoResponse               = "infoResponse\n"
	clientToMasterGetServers                 = "getservers "
	clientToMasterGetServersExtended         = "getserversExt "
	masterToClientGetServersResponse         = "getserversResponse"
	masterToClientGetServersExtendedResponse = "getserversExtResponse"
	masterToClientEndOfTransmission          = "EOT\x00\x00\x00"
	maxPacketSizeIn                          = 2048
	maxPacketSizeOut                         = 1400
	challengePostfix                         = separator + "challenge" + separator
	emptyString                              = " empty"
	fullString                               = " full"
	ipv4String                               = " ipv4"
	ipv6String                               = " ipv6"
	ipv6Prefix                               = "/"
	ipv4Prefix                               = "\\"
)

func InitNetwork(s *State, o *Options) {
	for _, address := range o.listenAddresses {
		addr, err := net.ResolveUDPAddr("udp",
			net.JoinHostPort(address, strconv.Itoa(int(o.dpmasterPort))))

		OkOrBail(err)

		conn, err := net.ListenUDP("udp", addr)

		OkOrBail(err)

		s.connections = append(s.connections, conn)
	}

	var err error // why do I need to add this hack? because neither = nor :=
	//would work on the next line otherwise
	s.Db, err = NewRedisBackend(o.dbUrl, o.dbAuth)

	OkOrBail(err)
}

func ListInterfaces() {
	interfaces, err := net.Interfaces()
	w := new(tabwriter.Writer)

	w.Init(os.Stdout, 0, 8, 0, '\t', 0)

	OkOrBail(err)

	fmt.Fprintln(w, "Index\tMTU\tName\tHardware-Address\tFlags")
	fmt.Fprintln(w, "\t\t\t\tMulticast|Point to Point|Loopback|Broadcast|Up")

	for i := 0; i < len(interfaces); i += 1 {
		tmp := interfaces[i]
		fmt.Fprintf(w, "%d\t%d\t%s\t%s\t%05b\n",
			tmp.Index, tmp.MTU, tmp.Name,
			tmp.HardwareAddr, tmp.Flags)
	}

	w.Flush()
}

func Run(s *State, o *Options) {
	for _, conn := range s.connections {
		s.Logger.Infof("listening on %s\n", conn)
		go Listen(s, conn)
	}
	for {
		time.Sleep(20 * time.Second)
	}
}

func ListenOnState(s *State) {
	Listen(s, s.connections[0])
}

func Listen(s *State, conn *net.UDPConn) {
	for {
		bytes_ := make([]byte, maxPacketSizeIn)

		nbytes, remote, err := conn.ReadFromUDP(bytes_)

		OkOrBail(err)

		if nbytes > 0 {
			go HandleMessage(conn, s, remote, bytes_[:nbytes])
		}
	}
}

func possiblyValidDpPacket(msg []byte) bool {
	return bytes.HasPrefix(msg, []byte(packetPrefix))
}

var (
	dispatcher = map[string]messageHandler{
		serverToMasterHeartbeat:          handleServerHeartbeat,
		serverToMasterInfoResponse:       handleServerInfo,
		clientToMasterGetServers:         handleClientGetServers,
		clientToMasterGetServersExtended: handleClientGetServersExtended,
	}
)

func HandleMessage(local *net.UDPConn, s *State, remote *net.UDPAddr, msg []byte) {
	//drop message if it doesn't comply with the darkplaces protocol
	if !possiblyValidDpPacket(msg) {
		return
	} else {
		msg = bytes.TrimPrefix(msg, []byte(packetPrefix))
	}

	known_message_type := false

	for key, dfunc := range dispatcher {
		if bytes.HasPrefix(msg, []byte(key)) {
			msg = bytes.TrimPrefix(msg, []byte(key))
			dfunc(local, s, remote, msg)
			known_message_type = true
		}
	}

	if !known_message_type {
		s.Logger.Infof("received unknown message typ: %s\n", msg)
	}
}

func handleServerHeartbeat(local *net.UDPConn, s *State, remote *net.UDPAddr,
	msg []byte) {
	if s.AcceptMode != AcceptAll && s.AcceptMode != AcceptServers {
		s.Logger.Debugf("m <-/- s (%s) Heartbeat", remote)
		return
	}
	challenge := string(s.Db.StoreHeartbeatChallenge(s.Rand, remote.String()))

	_, err := local.WriteToUDP([]byte(packetPrefix+masterToServerGetInfo+challenge), remote)

	OkOrLog(err)

	s.Logger.Debugf("m <- s (%s) Heartbeat\n", remote)
	s.Logger.Debugf("m -> s (%s) getInfo %s\n", remote, string(challenge))
	os.Stdout.Sync()
}

func handleServerInfo(local *net.UDPConn, s *State, remote *net.UDPAddr, msg []byte) {
	if s.AcceptMode != AcceptAll && s.AcceptMode != AcceptServers {
		s.Logger.Debugf("m <-/- s (%s) InfoResponse", remote)
		return
	}
	fields, err := ParseInfoResponse(msg)

	if s.Db.IsIgnored(*remote) || s.Db.IsBanned(*remote) {
		s.Logger.Infof("Ignored/Banned server %s tried to heartbeat\n", remote.String())
		return
	}

	OkOrLog(err)
	ping_addr := s.Db.PayloadForHeartbeatChallenge(s.Rand, fields["challenge"])
	if ping_addr != remote.String() {
		s.Logger.Warn("Warning: remote addr ", remote.String(), " doesn't match the recorded address ", ping_addr, " -- skipping info")
		return
	}

	s.Logger.Debugf("m <- s (%s) infoResponse %s\n", remote, string(msg))
	os.Stdout.Sync()

	s.Db.AddServerToPool(string(fields["gamename"]), msg, remote.String())

}

func sendGetServersResponse(local *net.UDPConn, s *State, remote *net.UDPAddr, servers [][]byte, responseType string) {
	current_packet := []byte(packetPrefix + responseType)
	servers_in_packet := 0

	for _, server := range servers {
		if len(current_packet)+1+len(server) > maxPacketSizeOut {
			local.WriteToUDP(current_packet, remote)
			current_packet := []byte(packetPrefix + responseType)
			servers_in_packet = 0

			s.Logger.Debug("m -> c ", responseType)
			s.Logger.Debug("    ", string(current_packet[4:]))
		}

		if len(server) == 4+2 {
			current_packet = append(current_packet, []byte(ipv4Prefix)...)
		} else {
			current_packet = append(current_packet, []byte(ipv6Prefix)...)
		}

		current_packet = append(current_packet, server...)
		servers_in_packet += 1
	}

	if servers_in_packet > 0 {
		local.WriteToUDP(current_packet, remote)
		s.Logger.Debug("m -> c ", responseType)
		s.Logger.Debug("    ", string(current_packet[4:]))
	}
}

func getGameName(msg []byte) string {
	return string(bytes.Split(bytes.TrimPrefix(bytes.TrimPrefix(msg, []byte(clientToMasterGetServersExtended)), []byte(clientToMasterGetServers)), []byte(" "))[0])
}

func binaryAddress(ip []byte, port int) []byte {
	result := make([]byte, 0)
	tmp := make([]byte, 2)
	result = append(result, ip...)
	binary.BigEndian.PutUint16(tmp, uint16(port))
	result = append(result, tmp...)
	return result
}

func makeBinaryAddress(addr net.UDPAddr) []byte {
	tmp := addr.IP.To4()
	if tmp != nil {
		return binaryAddress(tmp, addr.Port)
	} else {
		return binaryAddress(addr.IP, addr.Port)
	}
}

type filterFunc func(net.UDPAddr) bool

func isIPv6(addr net.UDPAddr) bool {
	return addr.IP.To4() == nil
}

func isIPv4(addr net.UDPAddr) bool {
	return !isIPv6(addr)
}

func filterAll(_ net.UDPAddr) bool {
	return true
}

func getFilteredServerPool(s *State, showEmpty, showFull, showIPv4, showIPv6 bool, gamename string) []net.UDPAddr {
	tmp := s.Db.GetServerPool(showEmpty, showFull, gamename)

	var fn filterFunc

	if showIPv4 && showIPv6 {
		fn = filterAll
	} else if showIPv6 {
		fn = isIPv6
	} else {
		fn = isIPv4
	}

	result := make([]net.UDPAddr, 0)

	for _, server := range tmp {
		if fn(server) {
			result = append(result, server)
		}
	}

	return result
}

func handleClientGet(local *net.UDPConn, s *State, remote *net.UDPAddr, msg []byte, extended bool) {
	var command string
	if extended {
		command = clientToMasterGetServersExtended
	} else {
		command = clientToMasterGetServers
	}

	if s.AcceptMode != AcceptAll && s.AcceptMode != AcceptClients {
		if extended {
			s.Logger.Debugf("m <-/- (%s) %s\n", remote, command)
		} else {
			s.Logger.Debugf("m <-/- (%s) %s\n", remote, command)
		}
		return
	}

	var servers [][]byte

	if s.Db.IsIgnored(*remote) || s.Db.IsBanned(*remote) {
		s.Logger.Infof("Ignored/Banned client %s tried to %s\n", remote, command)
		servers = make([][]byte, 0)
	} else {
		showEmpty := bytes.Contains(msg, []byte(emptyString))
		showFull := bytes.Contains(msg, []byte(fullString))
		showIPv4 := bytes.Contains(msg, []byte(ipv4String)) || !extended
		showIPv6 := bytes.Contains(msg, []byte(ipv6String)) && extended
		gamename := getGameName(msg)

		tmp := getFilteredServerPool(s, showEmpty, showFull, showIPv4, showIPv6, gamename)

		for _, address := range tmp {
			servers = append(servers, makeBinaryAddress(address))
		}
	}

	servers = append(servers, []byte(masterToClientEndOfTransmission))

	s.Logger.Debugf("m <- c (%s) %s %s\n", remote, command, msg)

	if extended {
		sendGetServersResponse(local, s, remote, servers, masterToClientGetServersExtendedResponse)
	} else {
		sendGetServersResponse(local, s, remote, servers, masterToClientGetServersResponse)
	}
}

func handleClientGetServers(local *net.UDPConn, s *State, remote *net.UDPAddr, msg []byte) {
	handleClientGet(local, s, remote, msg, false)
}

func handleClientGetServersExtended(local *net.UDPConn, s *State, remote *net.UDPAddr, msg []byte) {
	handleClientGet(local, s, remote, msg, true)
}
