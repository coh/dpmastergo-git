package dpmastergo

import (
	"fmt"
)

func PrintBanner() {
	fmt.Printf("\n"+
		"dpmastergo, an open master server\n"+
		"(version: %s, compiled on %v)\n", Version, BuildTime)
}
