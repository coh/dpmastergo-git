package dpmastergo

import "testing"

const (
	tries = 100000
)

func TestMakechallenge(t *testing.T) {
	s, _ := NewState()
	counts := make(map[int]int)
	chars := make(map[byte]int)

	for i := 0; i < tries; i += 1 {
		tmp := makechallenge(s.Rand)
		counts[len(tmp)] += 1

		for _, c := range tmp {
			chars[c] += 1
		}
	}

	sum := 0

	for i := challengeMinLength; i <= challengeMaxLength; i += 1 {
		t.Log(i, counts[i])
		sum += counts[i]
	}

	if sum != tries {
		t.Error("sum's wrong for makechallenge count\n")
	}

	if !(chars['\\'] == 0 && chars[';'] == 0 && chars['"'] == 0 &&
		chars['%'] == 0 && chars['/'] == 0) {
		t.Error("invalid characters were generated\n")
	}
}

func TestMakechallengeIsUnique(t *testing.T) {
	s, _ := NewState()
	counts := make(map[string]int)

	for i := 0; i < 10000; i += 1 {
		tmp := makechallenge(s.Rand)

		counts[string(tmp)] += 1
	}

	for token, count := range counts {
		if count != 1 {
			t.Error("duplicated token", token)
		}
	}
}
