package dpmastergo

import (
	"net"
	"strconv"
	"time"

	"github.com/garyburd/redigo/redis"
)

type RedisBackend struct {
	redis_details  string
	redis_password string
	client_only    bool
	pool           *redis.Pool
}

const (
	maxIdleCount    = 10
	maxActiveCount  = 0 // unlimited
	timeoutDuration = 20 * time.Minute
)

func newRedisPool(server, password string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     maxIdleCount,
		MaxActive:   maxActiveCount,
		IdleTimeout: timeoutDuration,
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if !OkOrLog(err) {
				return nil, err
			}

			if len(password) > 0 {
				_, err = c.Do("AUTH", password)
				if !OkOrLog(err) {
					return nil, err
				}
			}

			return c, nil
		},
	}
}

func NewRedisBackend(url, auth string) (*RedisBackend, error) {
	b := new(RedisBackend)

	b.redis_details = url
	b.redis_password = auth
	b.client_only = false //TODO

	b.pool = newRedisPool(url, auth)

	return b, nil
}

const (
	IndexChallenge = iota // glorified buffer for heartbeat challenges
	IndexStatus           // status info from servers
	IndexIgnore           // temporarily ignored IPs
	IndexBan              // permanently ignored IPs

	serversEmpty     = "es"
	serversFull      = "fs"
	serversPopulated = "ps"
)

func makechallenge(r CsPrng) []byte {
	clen := int(r.IntRange(MinChallengeLength, MaxChallengeLength))
	res := make([]byte, 0)
	for len(res) < clen {
		tmp := make([]byte, 64)
		_, err := r.Read(tmp)
		OkOrBail(err)

		for i := 0; i < len(tmp); i += 1 {
			tmp[i] = 0x21 + (tmp[i] & 0x7f)
			if tmp[i] != '\\' && tmp[i] != ';' && tmp[i] != '"' &&
				tmp[i] != '%' && tmp[i] != '/' && tmp[i] <= 0x7e {
				res = append(res, tmp[i])
			}
		}
	}

	return res[:clen]
}

func (b *RedisBackend) Get() redis.Conn {
	return b.pool.Get()
}

func (b *RedisBackend) StoreHeartbeatChallenge(rng CsPrng, payload string) []byte {

	challenge := makechallenge(rng)
	r := b.Get()

	_, err := r.Do("SELECT", IndexChallenge)

	OkOrLog(err)

	_, err = r.Do("SET", challenge, payload, "EX", int(ChallengeTimeout.Seconds()), "NX")

	OkOrLog(err)

	return challenge
}

func (b *RedisBackend) PayloadForHeartbeatChallenge(_ CsPrng, key []byte) string {
	r := b.Get()

	_, err := r.Do("SELECT", IndexChallenge)

	OkOrLog(err)

	res, err := redis.String(r.Do("GET", key))

	if !OkOrLog(err) {
		return ""
	}

	return res
}

func numSlots(fields map[string][]byte) int {
	result, err := strconv.Atoi(string(fields["sv_maxclients"]))

	OkOrLog(err)
	return result
}

func numClients(fields map[string][]byte) int {
	result, err := strconv.Atoi(string(fields["clients"]))

	OkOrLog(err)
	return result
}

func numBots(fields map[string][]byte) int {
	result, err := strconv.Atoi(string(fields["bots"]))

	//Warsow omits the bots field if there are no bots
	if !OkOrLog(err) {
		result = 0
	}
	return result
}

func isFull(state []byte) bool {
	fields, err := ParseInfoResponse(state)

	OkOrLog(err)
	return numSlots(fields)-(numClients(fields)-numBots(fields)) == 0
}

func isEmpty(state []byte) bool {
	fields, err := ParseInfoResponse(state)

	OkOrLog(err)
	return numClients(fields)-numBots(fields) <= 0
}

func (b *RedisBackend) removeServerFromAllPools(key string) {
	r := b.Get()

	_, err := r.Do("SELECT", IndexStatus)

	OkOrBail(err)

	r.Do("DEL", key)

	r.Do("SREM", serversEmpty, key)
	r.Do("SREM", serversFull, key)
	r.Do("SREM", serversPopulated, key)
}

func make_gameip(gamename, remoteAddress string) string {
	return gamename + "|" + remoteAddress
}

func (b *RedisBackend) AddServerToPool(gamename string, inforesponse []byte, remoteAddress string) {
	r := b.Get()

	_, err := r.Do("SELECT", IndexStatus)

	OkOrBail(err)

	is_empty, is_full := isEmpty(inforesponse), isFull(inforesponse)

	if is_empty && is_full {
		return //we don't accept schroedinger servers
	}

	b.removeServerFromAllPools(remoteAddress)

	var serverSet string

	if is_empty {
		serverSet = serversEmpty
	} else if is_full {
		serverSet = serversFull
	} else {
		serverSet = serversPopulated
	}

	_, err = r.Do("SADD", serverSet, remoteAddress)
	OkOrLog(err)
	_, err = r.Do("SET", make_gameip(gamename, remoteAddress), inforesponse, "EX", int(StatusTimeout.Seconds()))
	OkOrLog(err)
}

func (b *RedisBackend) GetServerPool(includeEmpty, includeFull bool, gamename string) []net.UDPAddr {
	r := b.Get()

	_, err := r.Do("SELECT", IndexStatus)

	OkOrBail(err)

	var tmp interface{}

	if includeEmpty && includeFull {
		tmp, err = r.Do("SUNION", serversEmpty, serversFull, serversPopulated)
	} else if includeEmpty {
		tmp, err = r.Do("SUNION", serversEmpty, serversPopulated)
	} else if includeFull {
		tmp, err = r.Do("SUNION", serversFull, serversPopulated)
	} else {
		tmp, err = r.Do("SMEMBERS", serversPopulated)
	}
	OkOrLog(err)

	res, err := redis.Values(tmp, err)
	result := make([]net.UDPAddr, 0)
	if !OkOrLog(err) {
		return result
	}

	for _, value := range res {
		bs, err := redis.String(value, err)
		OkOrLog(err)

		exists, err := redis.Bool(r.Do("EXISTS", make_gameip(gamename, bs)))
		OkOrLog(err)
		if exists {
			addr, err := net.ResolveUDPAddr("udp", bs)
			OkOrLog(err)
			result = append(result, *addr)
		}
	}

	return result
}

func (b *RedisBackend) isOnIgnore(ip net.IP, index int) bool {
	r := b.Get()

	_, err := r.Do("SELECT", index)
	OkOrBail(err)

	bans, err := redis.Strings(r.Do("KEYS", "*"))
	OkOrLog(err)

	for _, ban := range bans {
		_, iprange, err := net.ParseCIDR(ban)
		OkOrLog(err)

		if iprange.Contains(ip) {
			return true
		}
	}

	return false
}

func (b *RedisBackend) IsIgnored(ip net.UDPAddr) bool {
	return b.isOnIgnore(net.ParseIP(ip.String()), IndexIgnore)
}

func (b *RedisBackend) IsBanned(ip net.UDPAddr) bool {
	return b.isOnIgnore(net.ParseIP(ip.String()), IndexBan)
}

func (b *RedisBackend) GetServerStatus(address, gamename string) []byte {
	r := b.Get()

	_, err := r.Do("SELECT", IndexStatus)
	OkOrBail(err)

	result, err := redis.Bytes(r.Do("GET", make_gameip(gamename, address)))
	if OkOrLog(err) {
		return result
	} else {
		return nil
	}
}
