package dpmastergo

import "testing"

func TestValidDpPacket(t *testing.T) {
	const (
		good = "\xff\xff\xff\xffheartbeat DarkPlaces\n"
		bad1 = "\xff\xff\xffgetinfo Darkplaces\n"
	)

	if !possibleValidDpPacket([]byte(good)) {
		t.Error("good got evaluated as bad\n")
	}

	if possibleValidDpPacket([]byte(bad1)) {
		t.Error("bad1 got evaluated as good\n")
	}
}
