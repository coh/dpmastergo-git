package dpmastergo

import (
	crypto_rand "crypto/rand"
	"math/rand"
	"net"
	"os"

	"github.com/kdar/factorlog"

	cc "bitbucket.org/coh/chacha_prng"
)

const (
	AcceptAll = iota
	AcceptServers
	AcceptClients
)

type State struct {
	Rand        *cc.ChaChaPrng
	rnd         *rand.Rand
	connections []*net.UDPConn
	Db          Backend
	AcceptMode  int
	Logger      *factorlog.FactorLog
}

func NewState(o *Options) (*State, error) {
	result := new(State)
	key := make([]byte, 32)
	nonce := make([]byte, 8)

	len_, err := crypto_rand.Read(key)

	result.Db = nil

	OkOrBail(err)

	if len_ != len(key) {
		os.Exit(1)
	}

	len_, err = crypto_rand.Read(nonce)

	OkOrBail(err)

	if len_ != len(nonce) {
		os.Exit(1)
	}

	result.Rand = cc.NewPrng(key, nonce)
	result.rnd = rand.New(result.Rand)

	if o.clientOnly {
		result.AcceptMode = AcceptClients
	} else if o.serverOnly {
		result.AcceptMode = AcceptServers
	}

	result.Logger = NewLog(os.Stderr, o.minimumLoglevel)

	return result, nil
}
