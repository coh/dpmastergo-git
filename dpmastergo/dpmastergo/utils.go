package dpmastergo

import (
	"bytes"
	"errors"
	"os"
	"path/filepath"
	"runtime"

	"github.com/kdar/factorlog"
)

//sane defaults that can be changed later on
var OkOrBail, OkOrLog func(err error) bool = InitOkOrHelper(factorlog.Criticalf, 1, true), InitOkOrHelper(factorlog.Errorf, 2, false)

type LoggingFunc func(format string, args ...interface{})

func InitOkOrHelper(lfunc LoggingFunc, callDepth int, exit bool) func(err error) bool {
	return func(err error) bool {
		if err != nil {
			_, path, line, ok := runtime.Caller(callDepth)
			if ok {
				_, filename := filepath.Split(path)
				lfunc("@ %s:%d: %s\n", filename, line, err)
			} else {
				lfunc("@ ???: %s\n", err)
			}

			if exit {
				os.Exit(1)
			}
		}
		return err == nil
	}
}

const (
	fieldSeparator = "\\"
)

func ParseInfoResponse(response []byte) (map[string][]byte, error) {
	values := bytes.Split(bytes.TrimPrefix(response, []byte(fieldSeparator)),
		[]byte(fieldSeparator))

	result := make(map[string][]byte)

	if len(values)%2 == 0 {
		for i := 0; i < len(values); i += 2 {
			result[string(values[i])] = values[i+1]
		}
	} else {
		return nil, errors.New("Invalid InfoResponse")
	}

	return result, nil
}

func ParseInfoResponseString(response []byte) (map[string]string, error) {
	result, err := ParseInfoResponse(response)

	if err != nil {
		return nil, err
	}

	string_result := make(map[string]string, len(result))

	for key, value := range result {
		string_result[key] = string(value)
	}

	return string_result, err
}
