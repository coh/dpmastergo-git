package dpmastergo

import (
	"io"

	"github.com/kdar/factorlog"
)

const loggerFormatString = `%{Color "magenta" "PANIC"}%{Color "magenta" "FATAL"}%{Color "red" "ERROR"}%{Color "yellow" "WARN"}%{Color "green" "INFO"}%{Color "cyan" "DEBUG"}%{Color "blue" "TRACE"}[%{Time}] [%{Severity}:%{ShortFile}:%{Line}] %{SafeMessage}%{Color "reset"}`

func InitLogging(l *factorlog.FactorLog) {
	OkOrBail = InitOkOrHelper(l.Criticalf, 1, true)
	OkOrLog = InitOkOrHelper(l.Errorf, 2, false)
}

func NewLog(out io.Writer, minimumLevel factorlog.Severity) *factorlog.FactorLog {
	result := factorlog.New(out, factorlog.NewStdFormatter(loggerFormatString))
	result.SetMinMaxSeverity(factorlog.Severity(minimumLevel), factorlog.PANIC)
	return result
}
