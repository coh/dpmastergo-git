package dpmastergo

//Options and Constants

import (
	"os"
	"strconv"
	"strings"

	"time"

	"github.com/docopt/docopt-go"
	"github.com/kdar/factorlog"
)

const (
	//loopback
	defaultAllowLoopBack = false
	//port hashing
	defaultHashPorts = false
	//throttling
	defaultThrottleEnable = false
	//queries before throttling
	defaultPreThrottle = 5
	//duration between queries while throttling
	defaultThrottleDelay    = 3 * time.Second
	defaultDpMasterPort     = 27950
	defaultMaxListenSockets = 8
	defaultMinLogLevel      = 4
)

type Options struct {
	enableThrottling   bool               //whether to throttle or not
	preThrottleQueries uint               //how many queries before throttling
	throttleDelay      time.Duration      //how many seconds between queries while throttling
	allowLoopback      bool               //allow servers on loopback interfaces?
	dpmasterPort       uint               //change dpmaster port?
	listenAddresses    []string           //addresses to listen on
	verbose            bool               //verbose output
	dbUrl              string             //database url, can contain the auth details
	dbAuth             string             //database auth, if not included in dbUrl
	clientOnly         bool               //client-only? i.e., only report status to clients, but don't accept heartbeat updates?
	serverOnly         bool               //server-only? i.e., only accept heartbeats and getinfo responses from servers, but don't accept client requests?
	minimumLoglevel    factorlog.Severity //minimum log level
}

func ParseOpts() *Options {
	usage := `dpmastergo.

Usage:
  dpmastergo [options] [--client-only|--server-only] [--listen=INTERFACE]...

Options:
  --client-only                    only accept client messages?
  --server-only                    only accept server messages?
  -L LOGLEVEL --loglevel=LOGLEVEL  set log level. Valid values are None, Trace, Debug, Info, Warn, Error [default: info]
  -p PORT --port=PORT              port to listen on [default: 27950]
  --db-auth=DBAUTH                 specify authentication for connecting to the backend [default: ]
  --db-url=DBURL                   specify url for connecting to the backend. [default: localhost:6379]
  --list-interfaces                list available interfaces and exit.
  --listen=INTERFACE               listen on the given interface
  -h --help                        Show this screen.
  --version                        Show version and exit.
`

	arguments, err := docopt.Parse(usage, nil, true, "dpmastergo "+FullVersion, false)
	OkOrBail(err)

	port, err := strconv.ParseUint(arguments["--port"].(string), 10, 16)
	OkOrBail(err)

	if arguments["--list-interfaces"].(bool) {
		ListInterfaces()
		os.Exit(0)
	}

	addresses := arguments["--listen"].([]string)
	if len(addresses) == 0 {
		addresses = append(addresses, "")
	}

	return &Options{
		dpmasterPort:    uint(port),
		listenAddresses: addresses,
		dbUrl:           arguments["--db-url"].(string),
		dbAuth:          arguments["--db-auth"].(string),
		minimumLoglevel: factorlog.StringToSeverity(strings.ToUpper(arguments["--loglevel"].(string))),
		clientOnly:      arguments["--client-only"].(bool),
		serverOnly:      arguments["--server-only"].(bool),
	}
}
