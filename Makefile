VERSION=0.1
REVISION=$(shell hg heads -q)
TIME=$(shell date --rfc-3339=seconds)

all: timestamp
	go get dpmastergo
	go build dpmastergo
	go get dpmastertools/dpmg-webjson
	go build dpmastertools/dpmg-webjson
	go get dpmastertools/dpmg-stats
	go build dpmastertools/dpmg-stats
	go get rcon2irc
	go build rcon2irc

timestamp:
	echo -e "package dpmastergo\n\nconst (\n\tVersion   = \"$(VERSION)\"\n\tRevision  = \"$(REVISION)\"\n\tBuildTime = \"$(TIME)\"\n)\n" > dpmastergo/dpmastergo/build.go
clean:
	$(RM) dpmastergo
